<?php

use Faker\Generator as Faker;

$factory->define(\App\Barcode::class, function (Faker $faker) {
    return [
        'number'=> $faker->numberBetween(1000000, 2000000),
        'dateTime'=>$faker->dateTime,
        'scheduleDeliveryDate'=>$faker->dateTime,
        'citySenderId'=>$faker->numberBetween(1, 100),
        'cityRecipientId'=>$faker->numberBetween(1, 100),
        'senderPhone'=>$faker->numberBetween(100, 200),
        'recipientPhone'=>$faker->numberBetween(100, 200),
        'weight'=>$faker->numberBetween(1, 100),
        'cost'=>$faker->numberBetween(52, 1000),
    ];
});

