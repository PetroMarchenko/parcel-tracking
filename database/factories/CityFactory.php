<?php

use Faker\Generator as Faker;

$factory->define(\App\City::class, function (Faker $faker) {
    return [

        'cityName' => $faker->city,
        'longitude' => $faker->longitude,
        'latitude' => $faker->latitude

    ];
});
