<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Scan extends Model
{
        public function whCreator(){
            return $this->hasOne(Warehouse::class, 'whId', 'whId');
        }

        public function barcode(){
            return $this->belongsTo(Barcode::class, 'id', 'barcode_id');
        }

//
//
//    public function citySender()
//    {
//        return $this->hasOne(City::class, 'id', 'citySenderId');
//    }



}
