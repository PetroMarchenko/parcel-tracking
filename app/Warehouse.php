<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    public function cityOwner(){
        return $this->belongsTo(City::class, 'cityId', 'cityId');
    }
}
