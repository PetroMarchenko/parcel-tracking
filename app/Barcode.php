<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barcode extends Model
{
    protected $fillable = ['number', 'phone'];

    public function scans()
    {
        return $this->hasMany(Scan::class, 'barcode', 'number');
    }

    public function citySender()
    {
        return $this->hasOne(City::class, 'cityId', 'citySenderId');
    }

    public function cityRecipient()
    {
        return $this->hasOne(City::class, 'cityId', 'cityRecipientId');
    }

}
