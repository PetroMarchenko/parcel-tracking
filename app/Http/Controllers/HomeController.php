<?php

namespace App\Http\Controllers;

use App\Barcode;
use App\City;
use App\Scan;
use App\Warehouse;
use FarhanWazir\GoogleMaps\GMaps;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    public function index()
    {
        return view('trackme');
    }

    public function search($number, $phone)
    {


        $result = Barcode::where([['number', '=', $number]])->with('citySender', 'cityRecipient')->first();

        if (!$result) {
            return response()->json(
                ['errors' => [
                    'ew' => ['Номер ЭН введен неверно']
                ]], 419);


        } else {

            if($phone == $result->senderPhone || $phone == $result->recipientPhone) {
                $scans = Scan::selectRaw('whId, MIN(scanDate) as "scanDate"')->distinct()->where('barcode', '=', $number)->with('whCreator.cityOwner')->groupBy('whId')->orderByRaw('MIN(scanDate)')->get();
                return [$result, $scans];
            } else {
                return response()->json(
                    ['errors' => [
                        'phone' => ['Номер телефона не соответствует накладной']
                    ]], 419);
            }
        }


    }

}
